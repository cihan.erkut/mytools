#include <Rcpp.h>
using namespace Rcpp;

//' Convert wavelengths to RGB
//'
//' This function creates RGB colors for given wavelengths of light.
//'
//' Any wavelength outside the human perception range (i.e., 380 to 750 nm) is
//' represented in black. A slight attenuation is added to the blue and red ends
//' of the spectrum. There is no perfect way to represent a wavelength in colors
//' on a screen, therefore this function is basically an approximation. Using
//' the \code{gamma} parameter, one can adjust the non-linearity of color
//' distribution.
//'
//' Based on \url{http://www.physics.sfasu.edu/astro/color/spectra.html}
//'
//' @param lambda A numeric vector of wavelengths in nm.
//' @param alpha A numeric vector of alpha values for the RGB color.
//' @param gamma Gamma correction for the screen.
//' @return A vector of RGB values.
//' @seealso \code{\link{rgb}} for R function to create RGB values.
//' @import Rcpp
//' @useDynLib mytools
//' @export
// [[Rcpp::export]]
RObject wavelength_to_rgb(const NumericVector &lambda,
                          const Nullable<NumericVector> &alpha = R_NilValue,
                          const double gamma = 0.8)
{
  // Check if lambda and alpha vectors have the same size, if alpha != NULL
  const int n = lambda.size();
  if(alpha.isNotNull()) {
    const NumericVector a(alpha);
    if(a.size() != n) {
      stop("lambda and alpha must be the same size");
    }
  }

  // Wavelength cannot be 0 or less
  if (min(lambda <= 0)) {
    stop("lambda must be greater than 0");
  }

  // Main loop to iterate over lambda and calculate color intensities
  NumericVector red(n);
  NumericVector green(n);
  NumericVector blue(n);
  double l, R, G, B, visual_attenuation;

  for(int i = 0; i < n; ++i) {
    l = lambda[i];

    // Reduce color intensity towards the limits of visual perception
    visual_attenuation = 1;
    if(l <= 420) {
      visual_attenuation = 0.3 + 0.01750 * (l - 380);
    } else if(l >= 700) {
      visual_attenuation = 0.3 + 0.00875 * (780 - l);
    }

    // Calculate the red, green and blue intensities in [0, 1] depending on the
    // wavelength range. If the color is not represented in that range, it
    // defaults to 0. Apply visual attenuation if possible.
    if (l >= 380 && l < 440) {
      R = ((440 - l) / (440 - 380)) * visual_attenuation;
      B = visual_attenuation;

      red[i] = pow(R, gamma);
      blue[i] = pow(B, gamma);
    }
    else if (l >= 440 && l < 490) {
      G = (l - 440) / (490 - 440);

      green[i] = pow(G, gamma);
      blue[i] = 1;
    }
    else if (l >= 490 && l < 510) {
      B = (510 - l) / (510 - 490);

      green[i] = 1;
      blue[i] = pow(B, gamma);
    }
    else if (l >= 510 && l < 580) {
      R = (l - 510) / (580 - 510);

      red[i] = pow(R, gamma);
      green[i] = 1;
    }
    else if (l >= 580 && l < 645) {
      G = (645 - l) / (645 - 580);

      red[i] = 1;
      green[i] = pow(G, gamma);
    }
    else if (l >= 645 && l <= 750) {
      R = visual_attenuation;

      red[i] = pow(R, gamma);
    }
  }

  // Generate the color using the R function rgb
  Function R_rgb("rgb");
  RObject out = R_rgb(_["red"] = red, _["green"] = green, _["blue"] = blue,
                      _["alpha"] = alpha, _["maxColorValue"] = 1);

  return out;
}